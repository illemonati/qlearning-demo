
use rand::{self, Rng};
use rand::rngs::ThreadRng;
use std::fmt;


#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum TaxiGameBoardState {
    Empty,
    Taxi,
    Person,
    Destination
}

impl fmt::Display for TaxiGameBoardState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> { 
        match self {
            Self::Empty => write!(f, " "),
            Self::Taxi => write!(f, "T"),
            Self::Person => write!(f, "P"),
            Self::Destination => write!(f, "D")
        }
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct TaxiGameBoard {
    pub board: Vec<Vec<TaxiGameBoardState>>,
    pub size: usize
}



impl TaxiGameBoard {
    pub fn new(size: usize) -> Self {
        let mut board = Self {
            board: vec![],
            size
        };
        board.randomize();
        board
    }
    pub fn randomize(&mut self) {
        let mut new_board = vec![vec![TaxiGameBoardState::Empty; self.size]; self.size];
        let states_to_set = [TaxiGameBoardState::Person, TaxiGameBoardState::Taxi, TaxiGameBoardState::Destination];
        let mut rng = rand::thread_rng();
        for state in &states_to_set {
            loop {
                let location_x: usize = rng.gen_range(0, self.size);
                let location_y: usize = rng.gen_range(0, self.size);
                if new_board[location_y][location_x] == TaxiGameBoardState::Empty {
                    new_board[location_y][location_x] = *state;
                    break;
                }
            }
        }
        self.board = new_board;
    }
}

impl fmt::Display for TaxiGameBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        for row in &self.board {
            for col in row {
                write!(f, " {} |", col)?;
            }
            writeln!(f)?;
            for i in 0..row.len() {
                write!(f, "----")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}


#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum TaxiGameMove {
    Up,
    Down,
    Left,
    Right
}


#[derive(Clone, Debug)]
pub struct TaxiGame {
    pub board: TaxiGameBoard
}


impl TaxiGame {
    pub fn new(size: usize) -> Self {
        Self {
            board: TaxiGameBoard::new(size)
        }
    }
}
